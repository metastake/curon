#!/bin/sh

if [ -z "$URL" ] || [ "$COOLDOWN" -lt 1 ]; then
  echo "Wrong inputs:"
  echo "URL: $URL"
  echo "COOLDOWN: $COOLDOWN"
  exit 1
fi

while true; do
  response=$(curl -sk "$URL")

  if [ $? -eq 0 ]; then
    echo "GET $URL:"
    echo "$response"
  else
    echo "Error getting $URL"
  fi

  sleep "$COOLDOWN"
done
