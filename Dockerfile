FROM curlimages/curl

ADD start.sh /usr/local/bin/start.sh

RUN ls /usr/local/bin

CMD ["start.sh"]